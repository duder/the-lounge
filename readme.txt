Welcome to My First Repo
-------------------------------
This repo is a practice repo I am using to learn Bitbucket.

Git Commands:

cd 							- change directory
mkdir 						- make directory
ls -al						- list all files in directory
git clone [url] [name]		- clone git repo
rm -irf [repo name]/		- remove repo
git status 					- list status of local repo
git add						- add files to repo
git commit -m [message]		- make a commit with message
git push -u origin master	- push committed changes to repo